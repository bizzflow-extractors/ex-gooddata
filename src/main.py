import os
import json
from logging import getLogger, INFO, basicConfig
from gd import GoodData

def download_insight(conf, insight, logger):
    logger.info("Logging in to GoodData project %s", insight["pid"])
    gd = GoodData(conf["user"], conf["password"], conf["domain"], insight["pid"])
    if hasattr(insight["afm"], "lower"):
        afm = json.loads(insight["afm"])
    else:
        afm = insight["afm"]
    logger.info("Executing insight")
    result = gd.execute_afm(afm)
    fname = os.path.join("/data/out/tables", "{}.csv".format(insight["name"]))
    logger.info("Storing output into %s", fname)
    gd.export_afm_result(result, fname, insight["name"])

def main():
    basicConfig(level=INFO, format="[{asctime}] {levelname:10} {message}", style="{")
    logger = getLogger(__name__)
    logger.info("Loading configuration")
    with open("/config/config.json", encoding="utf-8") as conf_fid:
        conf = json.load(conf_fid)
    for insight in conf["insights"]:
        logger.info("Running download for insight %s", insight["name"])
        download_insight(conf, insight, logger)

if __name__ == "__main__":
    main()
